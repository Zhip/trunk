#pragma once
#include "ViscoelasticPM.hpp"
#include <core/PartialEngine.hpp>
#include <boost/unordered_map.hpp>
#include <functional>

namespace yade { // Cannot have #include directive inside.

class ViscElUDMat : public ViscElMat {
public:
	virtual ~ViscElUDMat();
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(ViscElUDMat,ViscElMat,"Material for extended viscoelastic model of contact as user defined.",
		((Real,kn1,0.0,,"Stiffness tension in first stage"))
		((Real,kn2,0.0,,"Stiffness tension in second stage"))
		((Real,cn1,0.0,,"Viscosity tension in first stage"))
		((Real,cn2,0.0,,"Viscosity tension in second stage"))
		((Real,kstens,0.0,,"Stiffness tension in tension stage"))
		((Real,cstens,0.0,,"Viscosity tension in tension stage"))
		((Real,Fyield,0.0,,"Yield force in tension stage"))
		((Real,bondn,0.0,,"Maximum tensile force in tension stage"))
		((Real,bonds,0.0,,"Maximum tensile force in tension stage")),
		createIndex();
	);
	// clang-format on
	REGISTER_CLASS_INDEX(ViscElUDMat, ViscElMat);
};
REGISTER_SERIALIZABLE(ViscElUDMat);

/// Interaction physics
class ViscElUDPhys : public ViscElPhys {
public:
	virtual ~ViscElUDPhys();
	Real R;
	// clang-format off
	YADE_CLASS_BASE_DOC_ATTRS_CTOR(ViscElUDPhys,ViscElPhys,"IPhys created from :yref:`ViscElUDMat`, for use with :yref:`Law2_ScGeom_ViscElUDPhys_Basic`.",
		((Real,kn1,0.0,,"Stiffness tension in first stage"))
		((Real,kn2,0.0,,"Stiffness tension in second stage"))
		((Real,cn1,0.0,,"Viscosity tension in first stage"))
		((Real,cn2,0.0,,"Viscosity tension in second stage"))
		((Real,kstens,0.0,,"Stiffness tension in tension stage"))
		((Real,cstens,0.0,,"Viscosity tension in tension stage"))
		((Real,UnloadF,0.0,,"Unload force in second stage"))
		((Real,UnloadS,0.0,,"Unload displacment in second stage"))
		((Real,Fyield,0.0,,"Yield force in tension stage"))
		((Real,bondn,0.0,,"Maximum tensile force in tension stage"))
		((Real,bonds,0.0,,"Maximum tensile force in tension stage"))
		((bool,bondCreated,false,,"BondState"))
		((bool,bondActive,false,,"BondState")),
		createIndex();
	)
	// clang-format on
	REGISTER_CLASS_INDEX(ViscElUDPhys, ViscElPhys);
};
REGISTER_SERIALIZABLE(ViscElUDPhys);

/// Convert material to interaction physics.
class Ip2_ViscElUDMat_ViscElUDMat_ViscElUDPhys : public Ip2_ViscElMat_ViscElMat_ViscElPhys {
public:
	void go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction) override;
	// clang-format off
	YADE_CLASS_BASE_DOC(Ip2_ViscElUDMat_ViscElUDMat_ViscElUDPhys,Ip2_ViscElMat_ViscElMat_ViscElPhys,"Convert 2 instances of :yref:`ViscElUDMat` to :yref:`ViscElUDPhys` using the rule of consecutive connection.");
	// clang-format on
	FUNCTOR2D(ViscElUDMat, ViscElUDMat);
};
REGISTER_SERIALIZABLE(Ip2_ViscElUDMat_ViscElUDMat_ViscElUDPhys);

/// Constitutive law
class Law2_ScGeom_ViscElUDPhys_Basic : public LawFunctor {
public:
	bool                                                                     go(shared_ptr<IGeom>&, shared_ptr<IPhys>&, Interaction*) override;
	FUNCTOR2D(ScGeom, ViscElUDPhys);
	// clang-format off
	YADE_CLASS_BASE_DOC(Law2_ScGeom_ViscElUDPhys_Basic,LawFunctor,"Extended version of Linear viscoelastic model with capillary parameters.");
	// clang-format on
	DECLARE_LOGGER;
};
REGISTER_SERIALIZABLE(Law2_ScGeom_ViscElUDPhys_Basic);

} // namespace yade
