#include "ViscoelasticUserDefined.hpp"
#include <lib/high-precision/Constants.hpp>
#include <core/Omega.hpp>
#include <core/Scene.hpp>
#include <core/State.hpp>
#include <pkg/common/Sphere.hpp>
#include <pkg/dem/ScGeom.hpp>

namespace yade { // Cannot have #include directive inside.

YADE_PLUGIN((ViscElUDMat)(ViscElUDPhys)(Ip2_ViscElUDMat_ViscElUDMat_ViscElUDPhys)(Law2_ScGeom_ViscElUDPhys_Basic));

/* ViscElUDMat */
ViscElUDMat::~ViscElUDMat() { }

/* ViscElUDPhys */
ViscElUDPhys::~ViscElUDPhys() { }

/* Ip2_ViscElUDMat_ViscElUDMat_ViscElUDPhys */
void Ip2_ViscElUDMat_ViscElUDMat_ViscElUDPhys::go(const shared_ptr<Material>& b1, const shared_ptr<Material>& b2, const shared_ptr<Interaction>& interaction)
{
	// no updates of an existing contact
	if (interaction->phys) return;
	shared_ptr<ViscElUDPhys> phys(new ViscElUDPhys());
	Calculate_ViscElMat_ViscElMat_ViscElPhys(b1, b2, interaction, phys);

	ViscElUDMat* mat1 = static_cast<ViscElUDMat*>(b1.get());
	ViscElUDMat* mat2 = static_cast<ViscElUDMat*>(b2.get());

	if (mat1->Fyield == mat2->Fyield and mat1->bondn == mat2->bondn and mat1->bonds == mat2->bonds) {
		phys->Fyield = mat1->Fyield;
		phys->bondn = mat1->bondn;
		phys->bonds = mat1->bonds;
	} else {
		throw runtime_error("Stiffness tension should be equal for both particles!.");
	}
	phys->kn1 = contactParameterCalculation(mat1->kn1, mat2->kn1);
	phys->kn2 = contactParameterCalculation(mat1->kn2, mat2->kn2);
	phys->cn1 = contactParameterCalculation(mat1->cn1, mat2->cn1);
	phys->cn2 = contactParameterCalculation(mat1->cn2, mat2->cn2);
	phys->kstens = contactParameterCalculation(mat1->kstens, mat2->kstens);
	phys->cstens = contactParameterCalculation(mat1->cstens, mat2->cstens);

	interaction->phys = phys;
}

/* Law2_ScGeom_ViscElUDPhys_Basic */
bool Law2_ScGeom_ViscElUDPhys_Basic::go(shared_ptr<IGeom>& _geom, shared_ptr<IPhys>& _phys, Interaction* I)
{
	Vector3r force = Vector3r::Zero();

	const id_t id1 = I->getId1();
	const id_t id2 = I->getId2();

	const ScGeom&        geom   = *static_cast<ScGeom*>(_geom.get());
	Scene*               scene2 = Omega::instance().getScene().get();
	ViscElUDPhys&       phys   = *static_cast<ViscElUDPhys*>(_phys.get());
	const BodyContainer& bodies = *scene2->bodies;

	if (not(phys.bondCreated) and geom.penetrationDepth >= 0) {
		phys.bondCreated = true;
		phys.bondActive  = false;
		Sphere* s1 = dynamic_cast<Sphere*>(bodies[id1]->shape.get());
		Sphere* s2 = dynamic_cast<Sphere*>(bodies[id2]->shape.get());
		if (s1 and s2) {
			phys.R = 2 * s1->radius * s2->radius / (s1->radius + s2->radius);
		} else if (s1 and not(s2)) {
			phys.R = s1->radius;
		} else {
			phys.R = s2->radius;
		}
	}
	// 计算断裂距离和最大拉力的距离
	Real Dtens = 0.0;
	Real Dloss = 0.0;
	Dtens = (phys.bondn-phys.Fyield) / phys.kn1;
	Dloss = Dtens - phys.bondn / phys.kn2;

	if (geom.penetrationDepth < 0) {
		if (phys.bondCreated and -geom.penetrationDepth <= Dloss) {
			if (not(phys.bondActive)) {
				phys.bondActive = true;
			}

			Real normalForceScalar = 0.0;
			Real dampForceScalar1  = 0.0;
			Real dampForceScalar2  = 0.0;

			const State& de1 = *static_cast<State*>(bodies[id1]->state.get());
			const State& de2 = *static_cast<State*>(bodies[id2]->state.get());

			Vector3r& shearForce = phys.shearForce;
			if (I->isFresh(scene2)) shearForce = Vector3r(0, 0, 0);
			const Real& dt = scene2->dt;
			shearForce = geom.rotate(shearForce);
            
			// Handle periodicity.
			const Vector3r shift2   = scene2->isPeriodic ? scene2->cell->intrShiftPos(I->cellDist) : Vector3r::Zero();
			const Vector3r shiftVel = scene2->isPeriodic ? scene2->cell->intrShiftVel(I->cellDist) : Vector3r::Zero();

			const Vector3r c1x = (geom.contactPoint - de1.pos);
			const Vector3r c2x = (geom.contactPoint - de2.pos - shift2);

			const Vector3r relativeVelocity = (de1.vel + de1.angVel.cross(c1x)) - (de2.vel + de2.angVel.cross(c2x)) + shiftVel;
			const auto     normalVelocity   = geom.normal.dot(relativeVelocity);
			const Vector3r shearVelocity    = relativeVelocity - normalVelocity * geom.normal;
			
			shearForce += phys.kstens * dt * shearVelocity;
			Vector3r shearForceVisc = Vector3r::Zero();

			if (shearForce.norm() > phys.bonds) {
				shearForceVisc = phys.cstens * shearVelocity;
			}

			dampForceScalar1 = -phys.cn1 * normalVelocity;
			dampForceScalar2 = -phys.cn2 * normalVelocity;

			if (phys.UnloadS==0.0){
				if (-geom.penetrationDepth <  Dtens){
					normalForceScalar=-geom.penetrationDepth * phys.kn1 + phys.Fyield;
					phys.normalForce = -(normalForceScalar + dampForceScalar1) * geom.normal;
				} else {
					normalForceScalar=(-geom.penetrationDepth - Dtens) * phys.kn2 + phys.bondn;
					phys.normalForce = -(normalForceScalar + dampForceScalar2) * geom.normal;
					
					phys.UnloadS = -geom.penetrationDepth;
					phys.UnloadF = normalForceScalar;
				}
			} else {
				if (-geom.penetrationDepth <  phys.UnloadS){
					normalForceScalar=-geom.penetrationDepth * phys.UnloadF / phys.UnloadS;
					phys.normalForce = -(normalForceScalar + dampForceScalar1) * geom.normal;
				} else {
					normalForceScalar=(-geom.penetrationDepth - phys.UnloadS) * phys.kn2 + phys.UnloadF;
					phys.normalForce = -(normalForceScalar + dampForceScalar2) * geom.normal;
					
					phys.UnloadS = -geom.penetrationDepth;
					phys.UnloadF = normalForceScalar;
				}
			}
			
			Vector3r force2 = Vector3r::Zero();
			force2 = phys.normalForce + shearForce + shearForceVisc;
			if (I->isActive) {
				addForce(id1, -force2, scene2);
				addForce(id2, force2, scene2);
			};
			return true;
		} else {
			return false;
		};
	};

	if (phys.bondActive) {
		phys.bondActive = false;
	}

	if (I->isActive) {
		Vector3r torque1 = Vector3r::Zero();
		Vector3r torque2 = Vector3r::Zero();
		if (computeForceTorqueViscEl(_geom, _phys, I, force, torque1, torque2)) {
			addForce(id1, -force, scene2);
			addForce(id2, force, scene2);
			addTorque(id1, torque1, scene2);
			addTorque(id2, torque2, scene2);
			return true;
		} else {
			return false;
		}
	}
	return true;
}

} // namespace yade
